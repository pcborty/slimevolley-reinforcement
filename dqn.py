import slimevolleygym
from tensorflow import keras
import tensorflow as tf
import numpy as np
from collections import deque
from gym import wrappers


env = slimevolleygym.SlimeVolleyEnv()
env.atari_mode = True
env.survival_bonus = True

input_shape = [env.observation_space.shape[0]]
n_outputs = env.action_space.n

model = keras.models.Sequential([
    keras.layers.Dense(64, activation="relu", input_shape=input_shape),
    keras.layers.Dense(32, activation="relu"),
    keras.layers.Dense(n_outputs)
])


def epsilon_greedy_policy(state, epsilon=0):
    # print("Epsilon greedy policy")
    if np.random.rand() < epsilon:
        return np.random.randint(n_outputs)

    Q_values = model.predict(state[np.newaxis])

    return np.argmax(Q_values[0])


replay_buffer = deque(maxlen=2000)


def sample_experiences(batch_size):
    # print("Sample experience")
    indices = np.random.randint(len(replay_buffer), size=batch_size)
    batch = [replay_buffer[index] for index in indices]

    states, actions, rewards, next_states, dones = [
        np.array([experience[field_index] for experience in batch]) for field_index in range(5)
    ]

    return states, actions, rewards, next_states, dones


def play_one_step(env, state, epsilon):
    # print("Play one step")
    action = epsilon_greedy_policy(state, epsilon)
    next_state, reward, done, info = env.step(action)
    replay_buffer.append((state, action, reward, next_state, done))

    return next_state, reward, done, info


batch_size = 64
discount_factor = 0.95
optimizer = keras.optimizers.Adam(learning_rate=0.001)
loss_fn = keras.losses.mean_squared_error


def training_step(batch_size):
    # print("Training step")
    experiences = sample_experiences(batch_size)
    states, actions, rewards, next_states, dones = experiences

    next_Q_values = model.predict(next_states)
    max_next_Q_values = np.max(next_Q_values, axis=1)

    target_Q_values = (rewards + (1 - dones) *
                       discount_factor * max_next_Q_values)

    mask = tf.one_hot(actions, n_outputs)

    # print("Gradient tape")
    with tf.GradientTape() as tape:
        all_Q_values = model(states)
        Q_values = tf.reduce_sum(all_Q_values * mask, axis=1, keepdims=True)
        loss = tf.reduce_mean(loss_fn(target_Q_values, Q_values))

    grads = tape.gradient(loss, model.trainable_variables)
    optimizer.apply_gradients(zip(grads, model.trainable_variables))


for episode in range(10000):
    obs = env.reset()
    total_reward = 0
    for step in range(600):
        epsilon = max(1 - episode / 500, 0.001)
        obs, reward, done, info = play_one_step(env, obs, epsilon)

        total_reward += reward
        if done:
            break

    print(f"Episode {episode} finished. Reward {total_reward}.")
    if episode > 100:
        training_step(batch_size)


wrapped_env = wrappers.Monitor(
    env,
    './videos/SlimeVolley_dqn/',
    force=True)

state = wrapped_env.reset()

episodes = 10

for episode in range(episodes):

    for step in range(1000):
        wrapped_env.render()

        action = model.predict(state[np.newaxis])
        next_state, reward, done, info = wrapped_env.step(np.argmax(action[0]))

        state = next_state

        if done:
            break

wrapped_env.close()
env.close()
