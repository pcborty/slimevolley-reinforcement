import slimevolleygym
from gym import wrappers
import time
from collections import defaultdict
import random

env = slimevolleygym.SlimeVolleyEnv()
env.survival_bonus = True


def convertKey(state, action):
    return tuple([(element * 10) // 1 for element in (state + action)])


def findMax(state, q_table):
    max_val = 0
    action = env.action_space.sample()

    for actionChoice in env.action_table:
        q_table_val = q_table[convertKey(state.tolist(), actionChoice)]

        if max_val < q_table_val:
            action = actionChoice
            max_val = q_table_val

    return (max_val, action)


episodes = 10000

# hyperparams
alpha = 0.1
gamma = 0.8
epsilon = 0.1

trackReward = 0

# key will be a 15 valued tuple > ((state), (action))
q_table = defaultdict(lambda: 0)

start_time = time.perf_counter()

for episode in range(episodes):
    episode_start_time = time.perf_counter()

    state = env.reset()
    episodeReward = 0

    for step in range(1000):
        # env.render()
        action = env.action_space.sample()

        if random.uniform(0, 1) > epsilon:
            _, action = findMax(state, q_table)

        next_state, reward, done, info = env.step(action)

        prev_val, _ = findMax(state, q_table)
        next_max_val, _ = findMax(next_state, q_table)

        new_val = ((1 - alpha) * prev_val) + \
            (alpha * (reward + (gamma * next_max_val)))

        state = state if type(state) == list else state.tolist()
        action = action if type(action) == list else action.tolist()

        q_table[convertKey(state, action)] = new_val
        state = next_state
        episodeReward += reward

        if done:
            episode_end_time = time.perf_counter()

            if episodeReward > trackReward:
                print(f"Reward increased! : {episodeReward}")
                trackReward = episodeReward

            print(
                f"""Episode {episode} finished after {step + 1} timesteps, 
                    in {episode_end_time - episode_start_time}s. q_table growth: {len(q_table)}.""")

            break

end_time = time.perf_counter()

print(f"Total elapsed time: {end_time - start_time}s")

wrapped_env = wrappers.Monitor(
    env,
    './videos/SlimeVolley_qlearning/',
    force=True)

episodes = 10

start_time = time.perf_counter()

for episode in range(episodes):
    episode_start_time = time.perf_counter()

    state = wrapped_env.reset()
    episodeReward = 0

    for step in range(800):
        wrapped_env.render()

        _, action = findMax(state, q_table)

        next_state, reward, done, info = wrapped_env.step(action)

        state = next_state

        if done:
            episode_end_time = time.perf_counter()

            print(
                f"""Episode {episode} finished after {step + 1} timesteps, 
                    in {episode_end_time - episode_start_time}s.""")

            break

end_time = time.perf_counter()

print(f"Total elapsed time: {end_time - start_time}s")
wrapped_env.close()
env.close()
